from django.db import models

# Create your models here.

class Adm_clave_tabla(models.Model):
	id_clave = models.CharField(max_length=50, primary_key=True, verbose_name='Clave')
	max_clave = models.IntegerField(verbose_name='Valor')
	descripcion = models.CharField(max_length=255)

	def __unicode__(self):
		return '%s' % (self.id_clave)

	def __str__(self):
		return '%s' % (self.id_clave)

	class Meta:
		db_table = u'adm_clave_tabla'

	class Admin:
		pass

