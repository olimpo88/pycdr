"""
Django settings for pycdr project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from databases import DATABASES
from extras import ALLOWED_HOSTS
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g&x5!bwmdqb+s8dh2nb#0%rqbs&+x_p*-err^b9i!4r2!s&(5x'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

LOGIN_URL='/common/login/'

# Application definition

INSTALLED_APPS = ( 
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'servers',
    'common',
)


#TEMPLATE_DIRS = (
#    '/www/pycdr/common/templates',
#    '/www/pycdr/pycdr/templates',
#)



TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'common/templates',
            'pycdr/templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth', 
                'django.template.context_processors.debug', 
                'django.template.context_processors.i18n', 
                'django.template.context_processors.media', 
                'django.template.context_processors.static', 
                'django.template.context_processors.tz', 
                'django.contrib.messages.context_processors.messages',
                #'django.core.context_processors.request',
                ],
        },
        #'TEMPLATE_DEBUG': True,
    },
]

#TEMPLATE_CONTEXT_PROCESSORS = (
#    'django.contrib.auth.context_processors.auth', 
#    'django.core.context_processors.debug', 
#    'django.core.context_processors.i18n', 
#    'django.core.context_processors.media', 
#    'django.core.context_processors.static', 
#    'django.core.context_processors.tz', 
#    'django.contrib.messages.context_processors.messages',
#    'django.core.context_processors.request',
#)








MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'pycdr.urls'

WSGI_APPLICATION = 'pycdr.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    "/www/pycdr/static",
)



