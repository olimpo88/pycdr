#from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls import include, url
#from recibos.views import *
from common.views import login
from pycdr.views import *
#admin.autodiscover()


urlpatterns = [
    url(r'^common/', include('common.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', login),
    url(r'^main/(?P<id_server>\d+)', main),
    url(r'^main/', main),
    url(r'^play_audio/(?P<id_server>\d+)/(?P<id_audio>\d+)', play_audio),
    url(r'^main_download/', main_download),
    url(r'^get_serverlist/(?P<id_organismo>\d+)', get_serverlist),
    url(r'^total_daily_calls/(?P<id_server>\d+)', get_total_daily_calls),
    url(r'^total_daily_calls/', main),
    url(r'^calls_io/(?P<id_server>\d+)', get_calls_io),
    url(r'^calls_io/', main),
    url(r'^listado_internos/(?P<id_server>\d+)', listado_internos),
    url(r'^categorias/(?P<id_server>\d+)', categorias),
    url(r'^statistical_graph/(?P<id_server>\d+)', get_statistical_graph),
    url(r'^statistical_graph/', main),   
]



#urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pycdr.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
#    url(r'^common/', include('common.urls')),
#    url(r'^admin/', include(admin.site.urls)),
#    url(r'^$', login),
#    url(r'^main/(?P<id_server>\d+)', main),
#    url(r'^main/', main),
#    url(r'^play_audio/(?P<id_server>\d+)/(?P<id_audio>\d+)', play_audio),
#    url(r'^main_download/', main_download),
#	url(r'^get_serverlist/(?P<id_organismo>\d+)', get_serverlist),
#	url(r'^total_daily_calls/(?P<id_server>\d+)', get_total_daily_calls),
#	url(r'^total_daily_calls/', main),
#	url(r'^calls_io/(?P<id_server>\d+)', get_calls_io),
#	url(r'^calls_io/', main),
#	url(r'^statistical_graph/(?P<id_server>\d+)', get_statistical_graph),
#	url(r'^statistical_graph/', main),	
	#url(r'^main/(?P<desde>[^/]+)/(?P<hasta>[^/]+)/(?P<origen>[^/]+)/(?P<destino>[^/]+)/(?P<c_origen>[^/]+)/(?P<c_destino>[^/]+)/(?P<estado>[^/]+)/(?P<pagina>[^/]+)', main_filter),
#)
