#encoding:utf-8
from django.contrib.auth import authenticate, login as djlogin, logout as djlogout
from django.contrib.auth.decorators import login_required
#from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, HttpResponseRedirect
#from django.utils import simplejson
from django.shortcuts import render_to_response, render
from django.template  import RequestContext
import json
import csv
#from servers.models import Activity_log
from django.db import models, connection, transaction
from servers.models import Server, Organismo, InternoCategoria
from common.utils import getForwardedFor
from datetime import datetime, timedelta
import json
import base64
from django.core import serializers
import subprocess
pyadmin=False


@login_required
def main(request, id_server=None):

	request.session['pyadmin']=pyadmin
	#Si tiene un solo servidor no le pido que lo seleccione
	srv = Server.objects.all()
	if len(srv)==1:
		id_server = srv[0].id_server


	#organismos= serializers.serialize('json', Organismo.objects.all())
	if pyadmin:
		servidor=Server.objects.get(id_server=id_server)
		organismos = [servidor.id_organismo.id_organismo,]
	else:
		organismos=Organismo.objects.all();
	#Si no envio el ID server le aviso
	if id_server==None:
		return render(request,'main_sin_id_server.html', { 'organismos': organismos })


	if "desde" in request.POST.keys():
		if request.POST["desde"]!='':
			desde=request.POST["desde"]
		else:
			desde=None	
		if request.POST["hasta"]!='':	
			hasta=request.POST["hasta"]
		else:
			hasta=None
		if request.POST["origen"]!='':
			origen=request.POST["origen"]
		else:
			origen=None
		if request.POST["destino"]!='':
			destino=request.POST["destino"]
		else:
			destino=None
		if request.POST["c_origen"]!='':
			c_origen=request.POST["c_origen"]
		else:
			c_origen=None
		if request.POST["c_destino"]!='':
			c_destino=request.POST["c_destino"]
		else:
			c_destino=None
		if request.POST["estado"]!='':
			estado=request.POST["estado"]
		else:
			estado=None
		if request.POST["pagina"]!='':
			pagina=int(request.POST["pagina"])
		else:
			pagina=1
	else:
		desde=None
		hasta=None
		origen=None
		destino=None
		c_origen=None
		c_destino=None
		estado=None
		pagina=1

	srv=Server.objects.get(pk=id_server)
	srv.astconnect()
	cdr=srv.get_cdr(desde, hasta, origen, destino, c_origen, c_destino, estado, pagina)

	#Paginacion
	actual=cdr['actual']
	cant_pages=cdr['cant_pages']
	lista=[]

	#Estas son las 2 casillas invisibles iniciales
	if cant_pages-actual < 1:
		if actual-4 > 0:
			lista.append(actual-4) 

	if cant_pages-actual < 2:
		if actual-3 > 0:
			lista.append(actual-3) 

	#Esto es para valores normales
	if actual-2 > 0:
		lista.append(actual-2) 

	if actual-1 > 0:
		lista.append(actual-1)

	lista.append(actual)

	if actual+1 <= cant_pages:
		lista.append(actual+1)

	if actual+2 <= cant_pages:
		lista.append(actual+2)

	#Estas son las 2 casillas invisibles finales
	if actual <= 2:
		#if actual-1 > 0:
		if actual+3<= cant_pages:
			lista.append(actual+3) 

	if actual <= 1:
		#if actual-2 > 0:
		if actual+4<= cant_pages:
			lista.append(actual+4) 

	#Antes de enviar los parametros a la vista los limpio
	if desde==None:
		desde=""
	if hasta==None:
		hasta=""
	if origen==None:
		origen=""
	if destino==None:
		destino=""
	if c_origen==None:
		c_origen=""
	if c_destino==None:
		c_destino=""

	return render(request, 'main.html', { 'srv': srv, 'organismos': organismos, 'cdr': cdr, 'paginacion': lista, 'desde': desde, 'hasta':hasta, 'origen':origen,'destino':destino, 'c_origen':c_origen, 'c_destino':c_destino, 'estado':estado, 'pag_actual': actual, 'pag_total': cant_pages })


@login_required
def play_audio(request,id_server,id_audio):
	srv=Server.objects.get(pk=id_server)
	srv.astconnect()

	salida=srv.getaudio(id_audio)
	
	response = HttpResponse()
	response.write(salida['file'])
	response['Content-Type'] ='audio/%s'%(salida['extension'])
	response['Content-disposition'] ="attachment"
	response['Content-Transfer-Encoding'] ="binary"	
	response['Content-Length'] =salida['size']
	return response

@login_required
def main_download(request):
	
	if "desde" in request.POST.keys():
		if request.POST["desde"]!='':
			desde=request.POST["desde"]
		else:
			desde=None	
		if request.POST["hasta"]!='':	
			hasta=request.POST["hasta"]
		else:
			hasta=None
		if request.POST["origen"]!='':
			origen=request.POST["origen"]
		else:
			origen=None
		if request.POST["destino"]!='':
			destino=request.POST["destino"]
		else:
			destino=None
		if request.POST["c_origen"]!='':
			c_origen=request.POST["c_origen"]
		else:
			c_origen=None
		if request.POST["c_destino"]!='':
			c_destino=request.POST["c_destino"]
		else:
			c_destino=None
		if request.POST["estado"]!='':
			estado=request.POST["estado"]
		else:
			estado=None
		if request.POST["id_server"]!='':
			id_server=request.POST["id_server"]
		else:
			estado=None
	else:
		desde=None
		hasta=None
		origen=None
		destino=None
		c_origen=None
		c_destino=None
		estado=None

	pagina=0
	srv=Server.objects.get(pk=id_server)
	srv.astconnect()
	cdr=srv.get_cdr(desde, hasta, origen, destino, c_origen, c_destino, estado, pagina)
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment;filename=export.csv'
	writer = csv.writer(response)
	for cdr in cdr['data']:
		writer.writerow([cdr[0],cdr[1],cdr[2],cdr[3],cdr[4],cdr[5],cdr[6],cdr[7]])
	return response

@login_required
def get_serverlist(request, id_organismo=None):
	servidores=Server.objects.filter(id_organismo=id_organismo)
	srv=[]
	for servidor in servidores:
		srv.append({'id_server':servidor.id_server,'nombre': servidor.name})
	return HttpResponse(json.dumps(srv), content_type='application/json')

@login_required
def get_total_daily_calls(request, id_server=None):

	request.session['pyadmin']=pyadmin
	
	if "desde" in request.POST.keys():
		if request.POST["desde"]!='':
			desde=request.POST["desde"]
		else:
			desde=None	
		if request.POST["hasta"]!='':	
			hasta=request.POST["hasta"]
		else:
			hasta=None
		if request.POST["origen"]!='':
			origen=request.POST["origen"]
		else:
			origen=None
		if request.POST["destino"]!='':
			destino=request.POST["destino"]
		else:
			destino=None
	else:
		desde=None
		hasta=None
		origen=None
		destino=None

	#Si tiene un solo servidor no le pido que lo seleccione
	srv = Server.objects.all()
	if len(srv)==1:
		id_server = srv[0].id_server
	#organismos= serializers.serialize('json', Organismo.objects.all())
	if pyadmin:
		servidor=Server.objects.get(id_server=id_server)
		organismos = [servidor.id_organismo.id_organismo,]
	else:
		organismos=Organismo.objects.all();

	srv=Server.objects.get(pk=id_server)
	srv.astconnect()
	data=srv.get_total_daily_calls(desde,hasta,origen,destino)
	totales=data['totales']
	porcentajes=data['porcentajes']

	if desde==None:
		desde=""
	if hasta==None:
		hasta=""
	if origen==None:
		origen=""
	if destino==None:
		destino=""		

	estado_totales=data['estado_totales']
	return render(request, 'llamadas_diarias.html',{ 'srv': srv, 'organismos': organismos, 'totales': totales, 'estado_totales': estado_totales, 'porcentajes':porcentajes ,'desde':desde, 'hasta':hasta, 'origen':origen, 'destino':destino })

@login_required
def get_calls_io(request, id_server=None):

	srv=Server.objects.get(pk=id_server)
	srv.astconnect()
	trunks=srv.asttrunks.split(",")


	request.session['pyadmin']=pyadmin	
	if "desde" in request.POST.keys():
		if request.POST["desde"]!='':
			desde=request.POST["desde"]
		else:
			desde=None	
		if request.POST["hasta"]!='':	
			hasta=request.POST["hasta"]
		else:
			hasta=None
		if request.POST["origen"]!='':
			origen=request.POST["origen"]
		else:
			origen=None
		if request.POST["destino"]!='':
			destino=request.POST["destino"]
		else:
			destino=None
		if request.POST["trunk"]!='':
			trunk=request.POST["trunk"]
			#print trunk
		else:
			trunk=trunks[0]			
	else:
		desde=None
		hasta=None
		origen=None
		destino=None
		trunk="all"

	data=srv.get_calls_io(desde,hasta,origen,destino,trunk)
	registros=data['data']
	dstchannel=data['dstchannel']
	channel=data['channel']
	estados=data['estado']

	if desde==None:
		desde=""
	if hasta==None:
		hasta=""
	if origen==None:
		origen=""
	if destino==None:
		destino=""	
	return render_to_response('calls_io.html', {'srv': srv, 'trunks':trunks, 'registros':registros, 'estados':estados, 'dstchannel':dstchannel ,'channel':channel , 'desde':desde, 'hasta':hasta, 'origen':origen, 'destino':destino, 'trunk':trunk})


@login_required
def categorias(request, id_server=None):
	srv=Server.objects.get(id_server=id_server)
	internos_categoria=InternoCategoria.objects.filter(id_server=srv)
	return render_to_response('categorias.html',{'srv':srv, 'internos_categoria':internos_categoria})


@login_required
def listado_internos(request, id_server=None):
	srv=Server.objects.get(id_server=id_server)
	
	if srv.es_local:
		#return_code = Popen(['nmap', '-sP', '10.114.150.0/24'],stdout=PIPE)
		#print return_code 
		proc = subprocess.Popen(["asterisk -rx 'sip show peers'"], stdout=subprocess.PIPE, shell=True)
		(out, err) = proc.communicate()
		internos=[]
		num_linea =0
		ultima_linea=len(out.splitlines())-1
		for linea in out.splitlines():
			#Descarto la primer linea
			print linea
			if num_linea!=0 and num_linea<ultima_linea:
				
				#Verifico si esta conectado
				if "OK" in linea:
					split = linea.split('                   ')
					split[2]="OK"
				else:
					split = linea.split('                       ')

					if "/" in split[0]:
						split = linea.split('                   ')
					split[2]="-"
				print "---> %s"%(split)
				internos.append(split)
			num_linea=num_linea+1
		print internos
	return render_to_response('listado_internos.html',{'srv':srv, 'internos':internos})



@login_required
def get_statistical_graph(request, id_server=None):
	srv=Server.objects.get(pk=id_server)
	srv.astconnect()

	request.session['pyadmin']=pyadmin	
	if "desde" in request.POST.keys():
		if request.POST["desde"]!='':
			desde=request.POST["desde"]
		else:
			desde=None	
		if request.POST["hasta"]!='':	
			hasta=request.POST["hasta"]
		else:
			hasta=None
		if request.POST["origen"]!='':
			origen=request.POST["origen"]
		else:
			origen=None
		if request.POST["destino"]!='':
			destino=request.POST["destino"]
		else:
			destino=None		
	else:
		desde=None
		hasta=None
		origen=None
		destino=None

	data=srv.get_statistical_graph(desde,hasta,origen,destino)


	if desde==None:
		desde=""
	if hasta==None:
		hasta=""
	if origen==None:
		origen=""
	if destino==None:
		destino=""	
	return render_to_response('graficos_estadisticos.html',{'srv': srv, 'data':data, 'desde':desde, 'hasta':hasta, 'origen':origen, 'destino':destino})