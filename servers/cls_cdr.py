#encoding:utf-8
from __future__ import division
import datetime
import math
import string
import time
import os
#import mysql.connector
import MySQLdb
from subprocess import Popen, PIPE
import base64

class Srv_cdr():
	"""
		Clase Base para la implementacion de los objecto Server.
	"""

	def astconnect(self):
		"""
			Conexion a la BBDD
		"""
		self.cnx=MySQLdb.connect(self.astip, self.astdbuser, self.astdbpasswd, self.astdbname)

	def get_total_tipo_llamada(self, fec_desde, fec_hasta):
		"""
			RECORDAR!!
			Los parametros son MAYOR o IGUAL a fec_desde
			y MENOR a fec_hasta
		"""
		cursor = self.cnx.cursor()
		query = ("select  disposition, count(*) from %s where calldate >= '%s' and calldate < '%s' group by disposition"%(self.astdbtable, fec_desde, fec_hasta))
		cursor.execute(query)
		res=[]
		res=cursor.fetchall()
		#for (disposition, cant) in cursor:
		cursor.close()
		#cnx.close()
		return res

	def get_tipo_llamada_por_dia(self, fec_desde, fec_hasta):
		"""
			RECORDAR!!
			Los parametros son MAYOR o IGUAL a fec_desde
			y MENOR a fec_hasta
		"""
		cursor = self.cnx.cursor()
		#query = ("select  disposition, count(*) from %s where calldate >= '%s' and calldate < '%s' group by disposition"%(self.astdbtable, fec_desde, fec_hasta))
		query = ("select  date(calldate), disposition, count(*) from %s where calldate >= '%s' and calldate < '%s' group by date(calldate), disposition"%(self.astdbtable, fec_desde, fec_hasta))

		cursor.execute(query)
		res=[]
		res=cursor.fetchall()
		#for (disposition, cant) in cursor:
		cursor.close()
		#cnx.close()
		return res

	def get_cdr(self, desde=None, hasta=None, origen=None, destino=None, c_origen=None, c_destino=None, estado=None, pagina=None):
		"""
			RECORDAR!!
			Los parametros son MAYOR o IGUAL a fec_desde
			y MENOR a fec_hasta
		"""
		if desde!=None and hasta !=None:
			#desde = string.replace(desde, "-", "/")
			conv=time.strptime(desde,"%d-%m-%Y")
			desde=time.strftime("%Y/%m/%d",conv)
			
			conv=time.strptime(hasta,"%d-%m-%Y")
			hasta=time.strftime("%Y/%m/%d",conv)

			where= "DATE(calldate) >= '%s' and DATE(calldate) <= '%s' "%(desde, hasta)
		else:
			where= "calldate is not null "
		
		if origen!=None:
			where= where + " AND src='%s'"%(origen)

		if destino!=None:
			where= where + " AND dst='%s'"%(destino)

		if c_origen!=None:
			where= where + " AND channel='%s'"%(c_origen)

		if c_destino!=None:
			where= where + " AND dstchannel='%s'"%(c_destino)

		if estado!=None and estado!='todas':
			where= where + " AND disposition='%s'"%(estado)

		if pagina==None:
			pagina=1

		#Con este arreglo uso la misma consulta para las descargas CSV
		limit=""
		rows_x_page=30
		row_start=rows_x_page*(pagina-1)
		if pagina!=0:
			limit="LIMIT %s, %s"%( row_start, rows_x_page)

		cursor = self.cnx.cursor()
		query = ("select  calldate, src, dst, channel, dstchannel, SEC_TO_TIME(duration), SEC_TO_TIME(billsec), disposition, id, userfield, RIGHT(userfield,3) as formato FROM %s where %s order by calldate desc %s "%(self.astdbtable, where,limit))
		cursor.execute(query)
		data=[]
		data=cursor.fetchall()

		cursor = self.cnx.cursor()
		query = ("select  count(*) FROM %s where %s"%(self.astdbtable, where))
		cursor.execute(query)
		total=cursor.fetchone()
		cant_pages = math.ceil(total[0]/rows_x_page)
		#resto=total[0]%rows_x_page
		#if resto != 0:
		#	cant_pages=cant_pages+1

		res={"data": data, "cant_pages": int(cant_pages) , "total": total, "actual":pagina }
		cursor.close()
		return res

	def get_total_daily_calls(self, desde=None, hasta=None, origen=None, destino=None,	estado=None):
		if desde!=None and hasta !=None:
			#desde = string.replace(desde, "-", "/")
			conv=time.strptime(desde,"%d-%m-%Y")
			desde=time.strftime("%Y/%m/%d",conv)
			
			conv=time.strptime(hasta,"%d-%m-%Y")
			hasta=time.strftime("%Y/%m/%d",conv)

			where= "DATE(calldate) >= '%s' and DATE(calldate) <= '%s'"%(desde, hasta)
		else:
			#where= "calldate is not null "
			where = " calldate > DATE_SUB(CURDATE(), INTERVAL 30 DAY)"

		if origen!=None:
			where= where + " AND src='%s'"%(origen)

		if destino!=None:
			where= where + " AND dst='%s'"%(destino)

		cursor = self.cnx.cursor()
		query = ("select  DATE(calldate) as fecha, count(*) as cantidad, SEC_TO_TIME(SUM(billsec)) as duracion FROM %s where %s group by DATE(calldate) order by DATE(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		totales=[]
		totales=cursor.fetchall()

		cursor = self.cnx.cursor()
		query = ("select  DATE(calldate) as fecha, count(*) as cantidad, disposition FROM %s where %s group by DATE(calldate), disposition order by DATE(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		data=[]
		data=cursor.fetchall()

		#Preparo el formateo para rellenar los estados que sean 0
		dia_anterior=""

		ANSWERED=0
		NO_ANSWER=0
		FAILED=0
		BUSY=0

		SUM_ANSWERED=0
		SUM_NO_ANSWER=0
		SUM_FAILED=0
		SUM_BUSY=0

		estados_totales=[]
		for row in data:

			if dia_anterior=="":
				dia_anterior = row[0]

			if dia_anterior!=row[0]:
				#Insertar
				estados_totales.append([dia_anterior, ANSWERED,NO_ANSWER,BUSY,FAILED])
				ANSWERED=0
				NO_ANSWER=0
				FAILED=0
				BUSY=0
				dia_anterior=row[0]

			if row[2]=="ANSWERED":
				ANSWERED=row[1]
				SUM_ANSWERED=SUM_ANSWERED+row[1]
			if row[2]=="NO ANSWER":
				NO_ANSWER=row[1]
				SUM_NO_ANSWER=SUM_NO_ANSWER+row[1]
			if row[2]=="BUSY":
				BUSY=row[1]
				SUM_BUSY=SUM_BUSY+row[1]
			if row[2]=="FAILED":
				FAILED=row[1]
				SUM_FAILED=SUM_FAILED+row[1]


		#Esto es para insertar el ultimo registro
		estados_totales.append([dia_anterior, ANSWERED,NO_ANSWER,BUSY,FAILED])

		#Calculo el porcentaje
		sum_total=SUM_ANSWERED+SUM_NO_ANSWER+SUM_FAILED+SUM_BUSY
		por_answered=0
		por_no_answer=0
		por_busy=0
		por_failed=0
		if sum_total!=0:
			por_answered=SUM_ANSWERED*100/sum_total
		if por_no_answer!=0:
			por_no_answer=SUM_NO_ANSWER*100/sum_total
		if por_busy!=0:
			por_busy=SUM_BUSY*100/sum_total
		if por_failed!=0:
			por_failed=float(SUM_FAILED*100/sum_total)
		porcentajes=[por_answered,por_no_answer,por_busy,por_failed]

		res={"totales": totales, "estado_totales": estados_totales, "porcentajes":porcentajes }
		return res


	def get_calls_io(self, desde=None, hasta=None, origen=None, destino=None,	trunk=None):
		if desde!=None and hasta !=None:
			#desde = string.replace(desde, "-", "/")
			conv=time.strptime(desde,"%d-%m-%Y")
			desde=time.strftime("%Y/%m/%d",conv)
			
			conv=time.strptime(hasta,"%d-%m-%Y")
			hasta=time.strftime("%Y/%m/%d",conv)

			where= "DATE(calldate) >= '%s' and DATE(calldate) < '%s'"%(desde, hasta)
		else:
			#where= "calldate is not null "
			where = " calldate > DATE_SUB(CURDATE(), INTERVAL 30 DAY)"

		if origen!=None:
			where= where + " AND src='%s'"%(origen)

		if destino!=None:
			where= where + " AND dst='%s'"%(destino)

		if trunk=="all" or trunk==None:
			where1= where
			where2= where
			where= where
		else:			
			where1= where + " AND channel like '%s' "%(trunk+"%")
			where2= where + " AND dstchannel like '%s' "%(trunk+"%")
			where= where + " AND (channel like '%s' OR dstchannel like '%s') "%(trunk+"%",trunk+"%")


		cursor = self.cnx.cursor()
		query = ("select  DATE(calldate) as fecha, src, dst, channel, dstchannel, disposition, SEC_TO_TIME(duration) as duracion, SEC_TO_TIME(billsec) as hablados FROM %s where %s order by DATE(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		data=cursor.fetchall()


		cursor = self.cnx.cursor()
		query = ("select  SUM(billsec), SEC_TO_TIME(SUM(billsec)), count(*) FROM %s where %s order by DATE(calldate)"%(self.astdbtable, where1))
		cursor.execute(query)
		data1=cursor.fetchall()

		cursor = self.cnx.cursor()
		query = ("select  SUM(billsec), SEC_TO_TIME(SUM(billsec)), count(*) FROM %s where %s order by DATE(calldate)"%(self.astdbtable, where2))
		cursor.execute(query)
		data2=cursor.fetchall()



		cursor = self.cnx.cursor()
		query = ("select  DATE(calldate) as fecha, count(*) as cantidad, disposition FROM %s where %s group by DATE(calldate), disposition order by DATE(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		data3=cursor.fetchall()

		SUM_ANSWERED=0
		SUM_NO_ANSWER=0
		SUM_FAILED=0
		SUM_BUSY=0
		for row in data3:
			if row[2]=="ANSWERED":
				SUM_ANSWERED=SUM_ANSWERED+row[1]
			if row[2]=="NO ANSWER":
				SUM_NO_ANSWER=SUM_NO_ANSWER+row[1]
			if row[2]=="BUSY":
				SUM_BUSY=SUM_BUSY+row[1]
			if row[2]=="FAILED":
				SUM_FAILED=SUM_FAILED+row[1]

		#Esto es para insertar el ultimo registro
		estados_totales=[SUM_ANSWERED,SUM_NO_ANSWER,SUM_BUSY,SUM_FAILED]
		res={"data": data, "channel": data1 ,"dstchannel": data2, "estado": estados_totales}
		return res
		
	def get_statistical_graph(self, desde=None, hasta=None, origen=None, destino=None,	trunk=None):
		if desde!=None and hasta !=None:
			#desde = string.replace(desde, "-", "/")
			conv=time.strptime(desde,"%d-%m-%Y")
			desde=time.strftime("%Y/%m/%d",conv)
			
			conv=time.strptime(hasta,"%d-%m-%Y")
			hasta=time.strftime("%Y/%m/%d",conv)

			where= "DATE(calldate) >= '%s' and DATE(calldate) < '%s'"%(desde, hasta)
		else:
			#where= "calldate is not null "
			where = " calldate > DATE_SUB(CURDATE(), INTERVAL 100 DAY)"

		if origen!=None:
			where= where + " AND src='%s'"%(origen)

		if destino!=None:
			where= where + " AND dst='%s'"%(destino)

		if trunk!=None:
			where1= where + " AND channel like '%s' "%(trunk+"%")
			where2= where + " AND dstchannel like '%s' "%(trunk+"%")
			where= where + " AND (channel like '%s' OR dstchannel like '%s') "%(trunk+"%",trunk+"%")


		cursor = self.cnx.cursor()
		query = ("SELECT  HOUR(calldate), count(*) FROM %s WHERE %s group by HOUR(calldate) order by HOUR(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		data=cursor.fetchall()
		diario=[]
		for i in range(24):
			llamadas=0
			for row in data:
				if i==row[0]:
					llamadas=row[1]
			diario.append([i,llamadas])

		cursor = self.cnx.cursor()
		query = ("SELECT  DAYOFWEEK(calldate), count(*) FROM %s WHERE %s group by DAYOFWEEK(calldate) order by DAYOFWEEK(calldate)"%(self.astdbtable, where))
		print query
		cursor.execute(query)
		data=cursor.fetchall()
		semanal=[]
		nombredia=""
		for i in range(1,8):
			if i==1:
				nombredia='Domingo'
			if i==2:
				nombredia='Lunes'
			if i==3:
				nombredia='Martes'
			if i==4:
				nombredia='Miércoles'
			if i==5:
				nombredia='Jueves'
			if i==6:
				nombredia='Viernes'
			if i==7:
				nombredia='Sábado'
			llamadas=0
			for row in data:
				if row[0]==i:
					llamadas=row[1]
			semanal.append([nombredia,llamadas])



		cursor = self.cnx.cursor()
		query = ("SELECT  DAY(calldate), count(*) FROM %s WHERE %s group by DAY(calldate) order by DAY(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		data=cursor.fetchall()
		mensual=[]
		for i in range(1,32):
			llamadas=0
			for row in data:
				if i==row[0]:
					llamadas=row[1]
			mensual.append([i,llamadas])


		cursor = self.cnx.cursor()
		query = ("SELECT  MONTH(calldate), count(*) FROM %s WHERE %s group by MONTH(calldate) order by MONTH(calldate)"%(self.astdbtable, where))
		cursor.execute(query)
		data=cursor.fetchall()
		anual=[]
		for i in range(1,13):
			if i==1:
				nombredia='Enero'
			if i==2:
				nombredia='Febrero'
			if i==3:
				nombredia='Marzo'
			if i==4:
				nombredia='Abril'
			if i==5:
				nombredia='Mayo'
			if i==6:
				nombredia='Junio'
			if i==7:
				nombredia='Julio'
			if i==8:
				nombredia='Agosto'
			if i==9:
				nombredia='Septiembre'
			if i==10:
				nombredia='Octubre'
			if i==11:
				nombredia='Noviembre'
			if i==12:
				nombredia='Diciembre'
			llamadas=0
			for row in data:
				if row[0]==i:
					llamadas=row[1]
			anual.append([nombredia,llamadas])


		res={"diario": diario, 'semanal':semanal, "mensual":mensual, "anual":anual}
		return res

	def getaudio(self,id_audio):
		ret=False
		
		#Traigo el registro
		cursor = self.cnx.cursor()
		query = ("select  userfield FROM %s where id=%s"%(self.astdbtable, id_audio))
		cursor.execute(query)
		data=cursor.fetchall()
		
		#Verifico si tiene el audio de esa llamda
		if len(data[0][0]):
			audio=data[0][0].split("audio:")
			audio[1]

			#Armo el nombre temporal
			audio_tmp=base64.b64encode(audio[1])+"."+audio[1][-3:]

			#Armo el comando y reemplazo lo que necesite
			astgetaudiocmd=self.astgetaudiocmd;
			astgetaudiocmd=astgetaudiocmd.replace("{SSHUSER}", self.sshuser)
			astgetaudiocmd=astgetaudiocmd.replace("{ASTIP}", self.astip)
			astgetaudiocmd=astgetaudiocmd.replace("{FILE}", audio[1])
			astgetaudiocmd=astgetaudiocmd.replace("{TEMP_FILE}", audio_tmp)
			astgetaudiocmd=astgetaudiocmd.replace("{TEMP_DIR}", self.asttmpdir)

			#Abro el archivo
			out = Popen(astgetaudiocmd, stdout=PIPE, shell=True).stdout.read()
			if len(out):
				self.lasterror = out
			else:
				#Envio el contenido y datos del archivo que voy a necesitar luego
				filename="%s/%s"%(self.asttmpdir,audio_tmp)
				f = open(filename,"rb") 
				ret={"file": f.read(), 'extension':audio[1][-3:], "size": os.path.getsize(filename)}
		
		return ret