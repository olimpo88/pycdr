#encoding:utf-8
from django.db import models
from cls_cdr import Srv_cdr

# Create your models here.

class Organismo(models.Model):
	id_organismo=models.AutoField(primary_key=True, verbose_name=u"Codigo")
	descripcion=models.CharField(max_length=50)

	def __unicode__(self):
		return '%s' % (self.descripcion)

	def __str__(self):
		return '%s' % (self.descripcion)

	class Meta:
		db_table=u'organismo'

	class Admin:
		pass


#class Server(models.Model, Srv_cdr):
class Server(models.Model, Srv_cdr):
	id_server=models.AutoField(primary_key=True, verbose_name=u"Codigo")
	name=models.CharField(max_length=50)
	habilitado=models.BooleanField(default=False)
	graba_llamadas=models.BooleanField(default=False)
	usa_categorias=models.BooleanField(default=False)
	es_local=models.BooleanField(default=False)
	sshuser=models.CharField(max_length=50)
	id_organismo=models.ForeignKey(Organismo, db_column='id_organismo', verbose_name='Organismo', on_delete=models.PROTECT)
	astip=models.CharField(max_length=50, null=True, blank=True, default=None)
	astdbname=models.CharField(max_length=50, null=True, blank=True, default=None)
	astdbuser=models.CharField(max_length=50, null=True, blank=True, default=None)
	astdbpasswd=models.CharField(max_length=50, null=True, blank=True, default=None)
	astdbtable=models.CharField(max_length=50, null=True, blank=True, default=None)
	asttrunks=models.CharField(max_length=250, null=True, blank=True, default=None)
	asttmpdir=models.CharField(max_length=255, null=True, blank=True, default=None)
	astgetaudiocmd=models.CharField(max_length=255, null=True, blank=True, default=None)


	def __unicode__(self):
		return '%s' % (self.name)

	def __str__(self):
		return '%s' % (self.name)

	class Meta:
		db_table=u'server'

	class Admin:
		pass


class InternoCategoria(models.Model):
	id_interno_categoria=models.AutoField(primary_key=True, verbose_name=u"Interno Categoria")
	id_server=models.ForeignKey(Server, db_column='id_server', verbose_name='Servidor', on_delete=models.PROTECT)
	interno=models.CharField(max_length=8)
	categoria=models.CharField(max_length=8)

	def __unicode__(self):
		return '%s - %s' % (self.interno,self.categoria)

	def __str__(self):
		return '%s - %s' % (self.interno,self.categoria)

	class Meta:
		db_table=u'interno_categoria'

	class Admin:
		pass